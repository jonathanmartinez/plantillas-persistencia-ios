//
//  ViewController.m
//  PlantillaParse
//
//  Created by Otto Colomina Pardo on 14/1/15.
//  Copyright (c) 2015 Universidad de Alicante. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBotonEntrar:(id)sender {
    NSString *userName = self.campoLogin.text;
    NSString *password = self.campoPassword.text;
    
    NSLog(@"Se ha pulsado el botón de 'login'");
    
}


@end
